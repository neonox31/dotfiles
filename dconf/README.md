# Tilix configuration

Currently Tilix configuration is stored under dconf.
Please use the following commands to dump/restore configuration :

`dconf dump /com/gexperts/Tilix > tilix.dconf`

`dconf load /com/gexperts/Tilix/ < tilix.dconf`

# Gnome configuration

Currently Gnome configuration is stored under dconf.
Please use the following commands to dump/restore configuration :

`dconf dump /org/gnome/ > gnome.dconf`

`dconf load /org/gnome/ < gnome.dconf`
