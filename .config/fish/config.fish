set fish_greeting

set fish_emoji_width 2

set -gx LANG en_US.UTF-8
set -gx LANGUAGE en_US

if test -d (brew --prefix)"/share/fish/completions"
    set -g -x fish_complete_path $fish_complete_path (brew --prefix)/share/fish/completions
end

if test -d (brew --prefix)"/share/fish/vendor_completions.d"
    set -g -x fish_complete_path $fish_complete_path (brew --prefix)/share/fish/vendor_completions.d
end

starship init fish | source

# Set autojump
set -gx OSTYPE (uname -a | tr '[:upper:]' '[:lower:]')
if test -f ~/.autojump/share/autojump/autojump.fish; . ~/.autojump/share/autojump/autojump.fish; end

# publish fish_private_mode
if test -n "$fish_private_mode"
    set -x FISH_PRIVATE_MODE $fish_private_mode
end

# Set PATH 
set -gx PATH $HOME/.local/bin \
	     ./node_modules/.bin \
	     $HOME/.yarn/bin \
	     $HOME/.cargo/bin \
	     /home/linuxbrew/.linuxbrew/bin \
	     $PATH

# Aliases
## System
alias ll='ls -lah'
alias rcp='rsync -avhr --info=progress2'
alias cb='xclip -selection clipboard'
alias cat='bat'
alias ispeed='speedtest'

# Direnv
set -x DIRENV_LOG_FORMAT ""
direnv hook fish | source
