function goland -d "overriding default goland by detaching it from terminal" -w goland
    ~/.local/bin/goland $argv > /dev/null 2>&1 & disown
end
