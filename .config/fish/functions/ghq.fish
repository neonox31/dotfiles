function ghq -d "overrides get look of ghq to simply cd" -w ghq
    if string match -q -- "get*-l*" "$argv"
    	set args (string split ' ' (string replace -r -- " -l| --look" "" "$argv"))
    	command ghq $args 2>&1 | tee /tmp/.ghq_out && cd (cat /tmp/.ghq_out | grep -Eo '(/[^/ ]*)+/?' | grep -v "//git" | head -1) && rm -rf /tmp/.ghq_out
    else
    	command ghq $argv
    end
end
