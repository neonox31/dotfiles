#!/bin/bash

set -e

export HOST="192.168.10.251"
export PORT="6443"
KUBECONFIG_PATH="${HOME}/.kube/config"

OLD_KUBECONFIG=$(cat "${KUBECONFIG_PATH}")
NEW_KUBECONFIG=$(ssh -o "StrictHostKeyChecking=no" rancher@${HOST} -- cat /etc/rancher/k3s/k3s.yaml)

if [ -n "${NEW_KUBECONFIG}" ]; then
  echo -n "${OLD_KUBECONFIG}" > "${KUBECONFIG_PATH}.old"

  export CERTIFICATE=$(yq e '.clusters[] | select(.name == "default") | .cluster.certificate-authority-data' - <<< "${NEW_KUBECONFIG}")
  export USER_CERTIFCATE=$(yq e '.users[] | select(.name == "default") | .user.client-certificate-data' - <<< "${NEW_KUBECONFIG}")
  export USER_KEY=$(yq e '.users[] | select(.name == "default") | .user.client-key-data' - <<< "${NEW_KUBECONFIG}")

  NEW_KUBECONFIG=$(yq e '(.clusters[] | select(.name == "k3s-server") | .cluster.certificate-authority-data) = env(CERTIFICATE)' - <<< "${OLD_KUBECONFIG}")
  NEW_KUBECONFIG=$(yq e '(.clusters[] | select(.name == "k3s-server") | .cluster.server) = "https://env(HOST):env(PORT)"' - <<< "${NEW_KUBECONFIG}")
  NEW_KUBECONFIG=$(yq e '(.users[] | select(.name == "k3s-server") | .user.client-certificate-data) = env(USER_CERTIFCATE)' - <<< "${NEW_KUBECONFIG}")
  NEW_KUBECONFIG=$(yq e '(.users[] | select(.name == "k3s-server") | .user.client-key-data) = env(USER_KEY)' - <<< "${NEW_KUBECONFIG}")

  echo -n "${NEW_KUBECONFIG}" > "${KUBECONFIG_PATH}"

  NEW_KUBECONFIG_B64=$(echo -n "${NEW_KUBECONFIG}" | base64 -w 0)

  PROJECTS="$(curl -qsSf -X GET --header "PRIVATE-TOKEN: ${LAB_CORE_TOKEN}" "${LAB_CORE_HOST}/api/v4/users/${LAB_CORE_USER}/projects?simple=true&owned=true" | jq -r '.[] | select(.tag_list[] | contains("k3s")) | "\(.id) \(.name)"')"
  while IFS= read -r PROJECT
  do
    PROJECT_ID=$(echo -n "${PROJECT}" | awk '{ print $1 }')
    PROJECT_NAME=$(echo -n "${PROJECT}" | awk '{ print $2 }')

    echo "Update variable for ${PROJECT_NAME} project..."
    curl -qsSf -X PUT --header "PRIVATE-TOKEN: ${LAB_CORE_TOKEN}" "${LAB_CORE_HOST}/api/v4/projects/${PROJECT_ID}/variables/KUBECONFIG_B64" --form "value=${NEW_KUBECONFIG_B64}" > /dev/null
  done <<< "${PROJECTS}"
else
  echo "Failed to retreive new kuneconfig..." && exit 1
fi
