#!/bin/bash

project_path=$(git remote get-url --push origin | sed -r 's@^(ssh|http(s)?)?(://)?(git\@)?([^/:]+)[/:](.*).git$@\6@')

if [ -z "${project_path}" ]; then
  echo -e "\e[31munable to identify project path\e[0m"
  exit 1
fi

project_id=$(curl -qsSf -X GET --header "PRIVATE-TOKEN: ${LAB_CORE_TOKEN}" "${LAB_CORE_HOST}/api/v4/projects/${project_path//\//%2F}" | jq '.id')

if [ -z "${project_id}" ]; then
  echo -e "\e[31munable to find project\e[0m"
  exit 1
fi

branch_name=$(git rev-parse --abbrev-ref HEAD)

if [ "${1}" == "--protect" ]; then
  ret_code=$(curl -s -o /dev/null -w "%{http_code}" -X POST --header "PRIVATE-TOKEN: ${LAB_CORE_TOKEN}" "${LAB_CORE_HOST}/api/v4/projects/${project_id}/protected_branches?name=${branch_name}")
  if [ "${ret_code}" -eq 201 ] || [ "${ret_code}" -eq 409 ]; then
    echo "${branch_name} branch is protected"	  
  else
    echo -e "\e[31man error occured while protecting ${branch_name} branch\e[0m"
    exit 1
  fi
elif [ "${1}" == "--unprotect" ]; then
  ret_code=$(curl -s -o /dev/null -w "%{http_code}" -X DELETE --header "PRIVATE-TOKEN: ${LAB_CORE_TOKEN}" "${LAB_CORE_HOST}/api/v4/projects/${project_id}/protected_branches/${branch_name}")
  if [ "${ret_code}" -eq 204 ] || [ "${ret_code}" -eq 404 ]; then
    echo "${branch_name} branch is unprotected"
  else
    echo -e "\e[31man error occured while unprotecting ${branch_name} branch\e[0m"
    exit 1
  fi
fi
