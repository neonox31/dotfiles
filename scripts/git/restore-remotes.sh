#!/bin/bash

set -e

repo_path=$(git rev-parse --show-toplevel 2> /dev/null)

if [ -z "${repo_path}" ]; then
  echo "repo path not found" && exit 1;
fi

if [ ! -e "${repo_path}/.gitremotes" ]; then
  echo ".gitremotes file not found in repo" && exit 1;
fi

while read -r remote; do
  remote_name=$(echo "${remote}" | awk '{ print $1 }')
  remote_url=$(echo "${remote}" | awk '{ print $2 }')
  set +e
	git remote add "${remote_name}" "${remote_url}"
	set -e
done < "${repo_path}/.gitremotes"