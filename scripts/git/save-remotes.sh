#!/bin/bash

set -e

repo_path=$(git rev-parse --show-toplevel 2> /dev/null)

if [ -z "${repo_path}" ]; then
  echo "repo path not found" && exit 1;
fi

git remote -v | awk '{ print $1" "$2 }' | uniq > "${repo_path}/.gitremotes"